import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/favorites.model.dart';

class NameTile extends StatelessWidget {
  final String name;
  final TextStyle _biggerFont = const TextStyle(fontSize: 18.0);

  const NameTile({Key key, @required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<FavoritesModel>(
      builder: (context, favoritesModel, child) {
        final bool alreadySaved = favoritesModel.contains(name);
        return ListTile(
          title: Text(
            name,
            style: _biggerFont,
          ),
          trailing: Icon(
            alreadySaved ? Icons.favorite : Icons.favorite_border,
            color: alreadySaved ? Colors.red : null,
          ),
          onTap: () {
            if (alreadySaved)
              favoritesModel.remove(name);
            else
              favoritesModel.add(name);
          },
        );
      },
    );
  }
}
