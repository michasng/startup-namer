import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/favorites.model.dart';
import 'pages/suggestions.page.dart';

void main() => runApp(
      ChangeNotifierProvider(
        builder: (context) => FavoritesModel(),
        child: MyApp(),
      ),
    );

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Startup Name Generator',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: SuggestionsPage(),
    );
  }
}
