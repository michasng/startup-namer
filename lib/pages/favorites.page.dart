import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import '../models/favorites.model.dart';
import '../widgets/nameTile.widget.dart';

class FavoritesPage extends StatefulWidget {
  final List<String> currentFavorites;

  FavoritesPage({
    Key key,
    @required this.currentFavorites,
  }) : super(key: key);

  @override
  FavoritesPageState createState() => FavoritesPageState();
}

class FavoritesPageState extends State<FavoritesPage> {
  final addNameController = TextEditingController();
  List<String> currentFavorites;

  @override
  void initState() {
    // needed bc widget.currentFavorites is immutable
    currentFavorites = <String>[];
    currentFavorites.addAll(widget.currentFavorites);
    addNameController.addListener(() => setState(() => {}));
    super.initState();
  }

  @override
  void dispose() {
    addNameController.dispose();
    super.dispose();
  }

  void addName(context) {
    var name = addNameController.text;
    addNameController.clear();
    try {
      if (name.isEmpty) throw 'Cannot add empty name';
      if (currentFavorites.contains(name))
        throw ('Cannot add duplicate name ' + name);

      var favoritesModel = Provider.of<FavoritesModel>(context, listen: false);
      favoritesModel.add(name);
      setState(() => currentFavorites.add(name));
    } catch (error) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(error),
        duration: Duration(seconds: 1),
      ));
    }
  }

  bool _isAddDisabled() {
    return addNameController.text.isEmpty;
  }

  @override
  Widget build(BuildContext context) {
    final Iterable<Widget> tiles = currentFavorites.map(
      (String name) => NameTile(
        name: name,
      ),
    );
    final List<Widget> divided = ListTile.divideTiles(
      context: context,
      tiles: tiles,
    ).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text('Saved Favorites'),
      ),
      body: Builder(
        builder: (context) => Stack(
          children: <Widget>[
            ListView(
              children: divided,
              padding: EdgeInsets.only(bottom: 64),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Card(
                margin: EdgeInsets.all(0),
                child: Padding(
                  padding: EdgeInsets.only(left: 12, right: 4),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: addNameController,
                          onSubmitted: (name) => addName(context),
                        ),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.playlist_add,
                          color: Colors.red,
                        ),
                        disabledColor: Colors.grey,
                        onPressed: () =>
                            _isAddDisabled() ? null : addName(context),
                      ),
                      Text(_isAddDisabled() ? 'd' : 'e'),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
