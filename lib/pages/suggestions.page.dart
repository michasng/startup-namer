import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import '../models/favorites.model.dart';
import '../widgets/nameTile.widget.dart';
import 'favorites.page.dart';

class SuggestionsPage extends StatefulWidget {
  @override
  SuggestionsPageState createState() => SuggestionsPageState();
}

class SuggestionsPageState extends State<SuggestionsPage> {
  final List<String> _suggestions = <String>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Suggestions'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.list), onPressed: _pushFavoritesPage),
        ],
      ),
      body: _buildSuggestions(),
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        if (i.isOdd) return Divider();
        final index = i ~/ 2;
        if (index >= _suggestions.length) {
          _suggestions.addAll(
              generateWordPairs().take(10).map((pair) => pair.asPascalCase));
        }
        return NameTile(name: _suggestions[index]);
      },
    );
  }

  void _pushFavoritesPage() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (BuildContext context) {
        // use Provider.of instead of Consumer, so the UI isn't always rebuilt
        var favoritesModel =
            Provider.of<FavoritesModel>(super.context, listen: false);
        var currentFavorites = favoritesModel.favorites;
        return new FavoritesPage(
          currentFavorites: currentFavorites,
        );
      }),
    );
  }
}
