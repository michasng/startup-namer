import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesModel extends ChangeNotifier {
  final List<String> _favorites = [];

  UnmodifiableListView<String> get favorites =>
      UnmodifiableListView(_favorites);

  FavoritesModel() {
    load();
  }

  bool contains(String name) => _favorites.contains(name);

  void add(String name) {
    print('Added name ' + name);
    _favorites.add(name);
    _save();
    notifyListeners();
  }

  void remove(String name) {
    print('Removed name ' + name);
    _favorites.remove(name);
    _save();
    notifyListeners();
  }

  void load() async {
    print('Loaded names');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final newFavs = (prefs.getStringList('favorites') ?? []);
    _favorites.clear();
    _favorites.addAll(newFavs);
    notifyListeners();
  }

  void _save() async {
    print('Saved names');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('favorites', _favorites);
  }
}
